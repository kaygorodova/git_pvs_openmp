#include "particle.h"
#include <iostream>
#include <vector>
#include <cmath>
#include <omp.h>
using namespace std;

double dr(double v){
	return v * t * 1000;
}

void init(Particle &q, ifstream &inst){
	q.place1 = new Vector_math;
	q.place2 = new Vector_math;
	inst>>q.m;
	inst>>q.q;
	inst>>q.place1 -> x;
	inst>>q.place1 -> y;
	inst>>q.place1 -> z;
	q.status = true;

	zero(q.f);
	zero(q.a);
	zero(q.v);

	return;
}

void in(vector<Particle> &m, ifstream &inst){
	int n;
	inst>>n;
	int i;
	m.resize(n);
	for(i = 0; i < n; i++)
		init(m[i], inst);

	return;
}

void force(Particle &q1, Particle &q2){
	Vector_math res;
	double f, r;
	r = len(sub(*(q1.place1), *(q2.place1)));
	f = k * q1.q * q2.q / (e * r * r);

	res = sub(*(q1.place1), *(q2.place1));
	res = norm(res);
	res = multConst(res, f);
    
    res = invert(res);

	q1.f = sum(q1.f, res);

	return;
}

void acceleration(Particle &q){
	q.a = multConst(q.f, 1. / q.m);
	return;
}

double run(double v, double a){
	return v * t + a * t * t / 2;
}

Vector_math run(Vector_math &v, Vector_math &a){
	Vector_math res;
	res = sum(multConst(v, t), multConst(a, t*t/2));
	return res;
}


void moving(Particle &q){
	Vector_math res;
	res = run(q.v, q.a);
	*q.place2 = sum(*(q.place1), multConst(res, 1000));
	return;
}

void speed(Particle &q){
	q.v = sum(q.v, multConst(q.a, t));
	return;
}

void collision(Particle &q1, Particle &q2){
	
	q1.q += q2.q;


	q1.v = multConst(q1.v, q1.m);
	q2.v = multConst(q2.v, q2.m);

	q1.v = sum(q1.v, q2.v);
	q1.v = multConst(q1.v, (1./(q1.m + q2.m)));
	q1.m += q2.m;

	*q1.place1 = multConst(sum(*q1.place1, *q2.place1), 1. / 2);

	q2.status = false;
	return;
}

void out(vector<Particle> &m){
	int i;
	cout<<"Parameters of particles"<<endl;
	for(i = 0; i < (int)m.size(); i++){
		if(m[i].status){
			cout<<i+1<<": ";
            cout<<"x="<<m[i].place1->x<<", y="<<m[i].place1->y<<", z="<<m[i].place1->z;
            cout<<", m="<<m[i].m<<", q="<<m[i].q;
            cout<<", vx="<<m[i].v.x<<", vy="<<m[i].v.y<<", vz="<<m[i].v.z<<endl;
        }
	}
	cout<<endl;
	return;
}

void step(vector<Particle> &m, bool flOut){
    
#pragma omp parallel for
    for(int i = 0; i < (int)m.size(); i++){
		if(m[i].status){
			for(int j = i+1; j < (int)m.size(); j++){
				if(m[j].status && len(sub(*m[i].place1, *m[j].place1)) <= dr(len(sub(m[i].v, m[j].v)))){
					collision(m[i], m[j]);
					
				}
 			}
		}
	}

	if(flOut)
		out(m);

#pragma omp parallel for
	for(int i = 0; i < (int)m.size(); i++){
		if(m[i].status && m[i].q){
			for(int j = 0; j < (int)m.size(); j++){
				if(i != j && m[j].status && m[j].q){
					force(m[i], m[j]);
				}
			}
		}
	}

#pragma omp parallel for
	for(int i = 0; i < (int)m.size(); i++){
		if(m[i].status){
			acceleration(m[i]);
			moving(m[i]);
			speed(m[i]);

            zero(m[i].f);                                                                                                       
            Vector_math *b;                                                     
            b = m[i].place1;                                                    
            m[i].place1 = m[i].place2;                                          
            m[i].place2 = b;
		}
	
	}
    

	return;
}
