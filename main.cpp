#include <iostream>
#include <vector>
#include "particle.h"
#include <fstream>
#include <stdio.h>
#include <stdlib.h>
using namespace std;


int main(int argc, char *argv[]){
	ifstream inst(argv[1], fstream::in);
	vector<Particle> m;
	in(m, inst);
	int n = atoi(argv[2]);
	while(n--){
		step(m, atoi(argv[3]));
	}
	return 0;
}
