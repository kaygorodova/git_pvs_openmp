#ifndef vector_math_h
#define vector_math_h
#define k 8.9875517873681764
#define e 1

struct Point{
	double x, y, z;
};

struct Vector_math{
	double x, y, z;
};



double len(const Vector_math &v);
Vector_math norm(const Vector_math &v);
Vector_math multConst(const Vector_math &v, double c);
Vector_math invert(const Vector_math &v);
Vector_math sum(const Vector_math &v1, const Vector_math &v2);
Vector_math sub(const Vector_math &z, const Vector_math &p);
void zero(Vector_math &v);

#endif
