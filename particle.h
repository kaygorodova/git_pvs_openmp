#ifndef particle_h
#define particle_h
#define k 8.9875517873681764
#define e 1
#define t 0.00001
#define dq 0.00001

#include "vector_math.h"

#include <iostream>
#include <vector>
#include <fstream>
using namespace std;


// ����� - ���, ���������� - ��, ����� - �


struct Particle{
	bool status;
	Vector_math *place1;
	Vector_math *place2;
	Vector_math v;
	Vector_math f;
	Vector_math a;
	int m;
	int q;
};


void init(Particle &q, ifstream &inst);
void in(vector<Particle> &m, ifstream &inst);
void force(Particle &q1, Particle &q2);
void acceleration(Particle &q);
void moving(Particle &q);
void speed(Particle &q);
void collision(Particle &q1, Particle &q2);

void out(vector<Particle> &m);

double run(double v, double a);
Vector_math run(Vector_math &v, Vector_math &a);

void step(vector<Particle> &m, bool flOut);

double dr(double v);

#endif
